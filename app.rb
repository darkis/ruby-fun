require 'require_all'
require 'json'

require_all 'base'
require_all 'util'
require_all 'characters'
require_all 'monsters'

character ||= nil
monster ||= nil
characters = [Rabbit.new]
dwary = DwemthysArray[
  IndustrialRaverMonkey.new,
  DwarvenAngel.new,
  AssistantViceTentacleAndOmbudsman.new,
  TeethDeer.new,
  IntrepidDecomposedCyclist.new,
  Dragon.new
]

puts "Welcome to the battle simulator! Please choose your character: "

counter = 0
characters.each { |chara|
  counter += 1
  puts "#{counter}. #{chara.class}"
}
choice = gets.chomp!.to_i
choice -= 1
character = characters[choice]
puts "You have chosen the [#{character.class}! Excellent!]"
puts "Now choose your enemy:"

counter = 0
dwary.each { |mon|
  counter += 1
  puts "#{counter}. #{mon.class}"
}

enemy_choice = gets.chomp!.to_i
enemy_choice -= 1
monster = dwary[enemy_choice]
puts "You have chosen the [#{monster.class}! Excellent!]"
puts "Time to battle!"
puts "What type of attack would you like to perform:"

counter = 0
character.actions.map { |key, value|
  counter += 1
  puts "#{counter}. #{key.capitalize}"
}
choose_action = gets.chomp!.to_i
choose_action -= 1
puts "#{character.actions.keys[choose_action].capitalize}! Nice choice!"
