class DwemthysArray < Array
  alias _inspect inspect
  def inspect; "#<#{self.class}#{_inspect}>"; end
  def method_missing(meth, *args)
    answer = first.send(meth, *args)
    if first.life <= 0
      shift

      puts "[Whoa. You decimated Dwemthy's Array!]" if empty?
      puts "[Get ready. #{first.class} has emerged.]" unless empty?
    end
    answer || 0
  end
end
